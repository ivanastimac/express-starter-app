const express = require('express');
const logger = require('morgan');
const app = express();
const port = 8002;
const customModule = require('./customModule');
const md5 = require('md5');

let auth_middleware = function(req, res, next) {
    
    if (!req.headers.authorization) {
        return res.status(403).json({ error: 'No credentials' });
      }

    if (req.headers.authorization === "790f2b152ad1010b8db473b206bb6725") {
        return res.status(401).send("Unauthorized");
    } else if (req.headers.authorization === "0972164370bb3a0c266fbd18c09c9e87") {
        const customAuthHeader = req.headers.authorization;
        req.customAuthHeader = customAuthHeader;
    }    

    next(); 
}

app.use(logger('dev'));
app.use(express.json());

app.get('/', (req, res) => {
    res.type('text/html');
    res.send('<h1> My First Express App - Author: Ivana Štimac </h1>');
});

app.get('/not-found', (req, res) => {
    res.type('text/html');
    res.status(404).send('<h3> Sorry. Page is not found. </h3>');
});

app.get('/error', function(req, res) {
    throw new Error('error');
});

app.get('/student-data', (req, res) => {
    
    let firstName = req.query.firstName;
    let lastName = req.query.lastName;
    let email = req.query.email;
    
    res.type('application/json');
    res.send({concatenatedString: customModule.concatenating(firstName, lastName, email)});

});

app.post('/authorization', (req, res) => {
    
    parsedBody = req.body;
    let concat = parsedBody.id + '_' + customModule.concatenating(parsedBody.firstName, parsedBody.lastName, parsedBody.email);
    const hash = md5(concat);
    
    res.type('application/json');
    res.header('Authorization', hash);
    res.json({hash: hash});

});

app.get('/private-route', auth_middleware, (req, res) => {
    
    res.type('application/json');

    if (req.customAuthHeader) {
        res.json({authHeaderExist: true, value: req.customAuthHeader});
    } else {
        res.json({authHeaderExist: false, value: ""});
    }

});

app.use(function(err, req, res, next) {
    console.error(err.stack);
    res.status(500).send('Something broke!');
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}!`);
});
